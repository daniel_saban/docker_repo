import requests
import os

input_sites = os.environ['INPUT']
url_list = input_sites.split()

for i in url_list:
    s = requests.get(i)
    print(s.text, '\n', '\n')
